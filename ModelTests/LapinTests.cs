﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Tests
{
    [TestClass()]
    public class LapinTests
    {

        [TestMethod()]
        public void CalculAgeTest()
        { 
            Lapin lapin = new Lapin(1, "panpan", new DateTime(2023,05,22), 0, 5, State.added, 1);
            int actual = lapin.CalculAge();
            int expected = -1;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculAgeTest2()
        {
            Lapin lapin = new Lapin(1, "panpan", new DateTime(2021, 05, 22), 0, 5, State.added, 1);
            int actual = lapin.CalculAge();
            int expected = 2;
            Assert.AreEqual(expected, actual);
        }
    }
}