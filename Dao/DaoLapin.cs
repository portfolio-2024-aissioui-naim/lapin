﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using Model;


namespace Utdl.Dao {
    public class DaoLapin {
        public void SaveChanges(List<Lapin> lapins) {
            for(int i=0;i<lapins.Count; i++) {
                Lapin lapin = lapins[i];
                switch(lapin.State) {
                    case State.added:
                        this.insert(lapin);
                        break;
                    case State.modified:
                        this.update(lapin);
                        break;
                    case State.deleted:
                        this.delete(lapin);
                        lapins.Remove(lapin);
                        break;
                }
            }
        }

        private void delete(Lapin lapin) {
            using(MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection()) {
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("delete from Lapin where id=@id",cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = lapin.Id;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void update(Lapin lapin) {
            using(MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection()) {
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("update Lapin set surnom=@surnom,age=@age,dateNaissance = @dateNaissance,position=@position,dossard=@dossard, idCourse=@idCourse where id=@id",cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@age",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@position",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@dossard",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@surnom",MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@idCourse", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@dateNaissance", MySqlDbType.DateTime));
                    cmd.Parameters["@id"].Value = lapin.Id;
                    cmd.Parameters["@age"].Value = lapin.CalculAge();
                    cmd.Parameters["@position"].Value = lapin.Position;
                    cmd.Parameters["@dossard"].Value = lapin.Dossard;
                    cmd.Parameters["@surnom"].Value = lapin.Surnom;
                    cmd.Parameters["@idCourse"].Value = lapin.IdCourse;
                    cmd.Parameters["@dateNaissance"].Value = lapin.DateNaissance;
                    cmd.ExecuteNonQuery();
                }
            }
            lapin.State = State.unChanged;
        }

        private void insert(Lapin lapin) {
            //int insertId = 0;
            using(MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection()) {
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("insert into Lapin(id,surnom,age,dateNaissance,position,dossard,idCourse) values(@id,@surnom,@age,@dateNaissance,@position,@dossard,@idCourse)", cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@idCourse",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@age",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@position",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@dossard",MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@surnom",MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@dateNaissance", MySqlDbType.DateTime));
                    cmd.Parameters["@id"].Value = lapin.Id;
                    cmd.Parameters["@age"].Value = lapin.CalculAge();
                    cmd.Parameters["@position"].Value = lapin.Position;
                    cmd.Parameters["@dossard"].Value = lapin.Dossard;
                    cmd.Parameters["@surnom"].Value = lapin.Surnom;
                    cmd.Parameters["@idCourse"].Value = lapin.IdCourse;
                    cmd.Parameters["@dateNaissance"].Value = lapin.DateNaissance;
                    cmd.ExecuteNonQuery();
                    //insertId = Convert.ToInt32(cmd.LastInsertedId);               
                }
            }
            lapin.State = State.unChanged;
            
        }

        public int GetIdLapin()
        {
            int id = 0;
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("select max(id) from Lapin", cnx))
                {
                    id = (int)cmd.ExecuteScalar();
                }
            }
            return id;
        }

        public List<Lapin> GetAll(int idCourse) {
            List<Lapin> lapins = new List<Lapin>();
            using(MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection()) {
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("select id,surnom,dateNaissance,age,position,dossard,idCourse from Lapin where idCourse=@idCourse",cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@idCourse", MySqlDbType.Int32));
                    cmd.Parameters["@idCourse"].Value = idCourse;
                    using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            lapins.Add(new Lapin(Convert.ToInt32(rdr["id"]),rdr["surnom"].ToString(),Convert.ToDateTime(rdr["dateNaissance"]),Convert.ToInt32(rdr["position"]),Convert.ToInt32(rdr["dossard"]),State.unChanged,Convert.ToInt32(rdr["idCourse"])));
                        }
                    }
                }
            }
            return lapins;
        }

        public int GetNombreLapin(int idCourse)
        {
            int nbLapin = 0;
            MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select count(*) from lapin where idCourse = @idCourse",cnx);
            cmd.Parameters.Add(new MySqlParameter("@idCourse", MySqlDbType.Int32));
            cmd.Parameters["@idCourse"].Value = idCourse;

            nbLapin = Convert.ToInt32(cmd.ExecuteScalar());
            cnx.Close();
            return nbLapin;
        }

        public Lapin GetById(int id) {
            using(MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection()) {
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("select id,surnom,age,position,dossard from Lapin where id=@id",cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = id;
                    using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            return new Lapin(Convert.ToInt32(rdr["id"]),rdr["surnom"].ToString(), Convert.ToDateTime(rdr["dateNaissance"]), Convert.ToInt32(rdr["position"]), Convert.ToInt32(rdr["dossard"]), State.unChanged, Convert.ToInt32(rdr["idCourse"]));
                        }
                    }
                }
            }
            throw new Exception("id non défini pour la table Lapin");
        }
    }
}