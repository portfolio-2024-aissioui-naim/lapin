﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utdl.Dao;
using Model;


namespace Utdl.View {
    public partial class FeditLapin:Form {
        private State state;
        private ListBox.ObjectCollection items;
        private int position;
        private int idCourse;

        public FeditLapin(State state, ListBox.ObjectCollection items, int position, int idCourse) {
            InitializeComponent();
      
            this.state = state;
            this.items = items;
            this.position = position;
            this.idCourse = idCourse;
            btnValider.Click += BtnValider_Click1;
            dtpDateNaiss.ValueChanged += DtpDateNaiss_ValueChanged;
            switch (state)
            {
                case State.added:
                    this.Text = "Création d'une lapin";
                    break;
                case State.modified:
                    Lapin lapin = (Lapin)items[position];
                    this.tbId.Text = Convert.ToString(lapin.Id);
                    this.tbSurnom.Text = lapin.Surnom;
                    this.tbAge.Text = Convert.ToString(lapin.CalculAge());
                    this.tbDossard.Text = lapin.Dossard.ToString();
                    this.tbPosition.Text = lapin.Position.ToString();
                    this.dtpDateNaiss.Value = lapin.DateNaissance;
                    
                    this.Text = "Modification d'une lapin";
                    break;
                case State.deleted:
                    this.Text = "Suppression d'une lapin";
                    break;
                case State.unChanged:
                    this.Text = "Consultation des lapins";
                    break;
                default:
                    break;
            }
        }

        private void DtpDateNaiss_ValueChanged(object sender, EventArgs e)
        {
            DateTime dateNaissance = dtpDateNaiss.Value;
            DateTime dateActuelle = DateTime.Now;
            int age = dateActuelle.Year - dateNaissance.Year;
            tbAge.Text = Convert.ToString(age);

        }

        private void BtnValider_Click1(object sender, EventArgs e)
        {
            if (Verification() == true)
            {
                switch (this.state)
                {
                    case State.added:
                        DaoLapin daoLapin = new DaoLapin();
                        int id = daoLapin.GetIdLapin();
                        id++;
                        items.Add(new Lapin(id,tbSurnom.Text, Convert.ToDateTime(dtpDateNaiss.Value), 0, Convert.ToInt32(tbDossard.Text), this.state, this.idCourse));
                        break;
                    case State.modified:
                        Lapin lapin = (Lapin)items[this.position];
                        lapin.Id = Convert.ToInt32(tbId.Text);
                        lapin.Surnom = tbSurnom.Text;
                        lapin.DateNaissance = dtpDateNaiss.Value;
                        lapin.SetAge(Convert.ToInt32(tbAge.Text));
                        lapin.Dossard = Convert.ToInt32(tbDossard.Text);
                        lapin.State = this.state;
                        
                        items[position] = lapin;
                        break;
                    case State.deleted:
                        break;
                    case State.unChanged:
                        // rien
                        break;
                    default:
                        break;
                }
                this.Close();
            }  
        }

        private bool Verification()
        {
            if (this.tbDossard.Text == "" || this.tbSurnom.Text == "")
            {
                MessageBox.Show("Il faut remplir les champs !");
                return false;
            }
            else
            {
                foreach (char c in tbAge.Text)
                {
                    if (!char.IsDigit(c))
                    {
                        MessageBox.Show("Error ! Surnom : string, Dossard : int, Age : int");
                        return false;
                    }
                }
                foreach (char c in tbDossard.Text)
                {
                    if (!char.IsDigit(c))
                    {
                        MessageBox.Show("Error ! Surnom : string, Dossard : int, Age : int");
                        return false;
                    }
                }
            }

            DateTime dateNaissance = dtpDateNaiss.Value;
            DateTime dateActuelle = DateTime.Now;
            int age = dateActuelle.Year - dateNaissance.Year;
            if (age < 2)
            {
                tbAge.Text = Convert.ToString(age);
                MessageBox.Show("Error ! Tu peux pas créer un lapin agée de moins de 2 ans");
                return false;
            }
            return true;
        }
    }
}
